from pyspark import SparkConf, SparkContext

conf = SparkConf().setMaster("local[*]").setAppName("RemoveDuplicates")
sc = SparkContext(conf = conf)

rdd = sc.textFile("datasets/minimized.csv")

## distinct function
save_results = rdd.distinct().cache()

save_results.saveAsTextFile("duplicate_removed")

sortedResults = rdd.distinct().collect()

for result in sortedResults:
    print(result)

